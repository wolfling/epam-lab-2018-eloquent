<?php
require_once './app.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::statement('SET foreign_key_checks = 0');

Capsule::schema()->drop('users');
Capsule::schema()->drop('posts');
Capsule::schema()->drop('tags');
Capsule::schema()->drop('post_tags');

Capsule::statement('SET foreign_key_checks = 1');