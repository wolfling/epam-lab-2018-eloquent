<?php
require_once './app.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->create('users', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name');
    $table->date('birthday')->nullable();
    $table->enum('gender', ['male', 'female']);
    $table->timestamps();
});

Capsule::schema()->create('posts', function (Blueprint $table) {
    $table->increments('id');
    $table->string('title');
    $table->string('slug');
    $table->string('description');
    $table->unsignedInteger('user_id')->index();
    $table->timestamps();
    $table->softDeletes();

    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
});

Capsule::schema()->create('tags', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name');
    $table->timestamps();
});

Capsule::schema()->create('post_tags', function (Blueprint $table) {
    $table->unsignedInteger('post_id')->index();
    $table->unsignedInteger('tag_id')->index();

    $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
    $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
});