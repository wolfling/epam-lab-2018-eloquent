<?php
require_once './app.php';

use Faker\Factory as Faker;
use app\Models\Post;
use app\Models\User;
use app\Models\Tag;

require_once './database/fixtures/truncate.php';

$faker = Faker::create();

for ($i = 0; $i < 10; $i++) {
    $tag = new Tag();
    $tag->name = $faker->text(20);
    $tag->save();
}

for ($i = 0; $i < 50; $i++) {
    $user = new User();
    $user->name = $faker->name;
    $user->birthday = $faker->dateTimeThisCentury;
    $user->gender = $faker->randomElement([User::GENDER_MALE, User::GENDER_FEMALE]);
    $user->save();
}

for ($i = 0; $i < 100; $i++) {
    $post = new Post();
    $post->title = $faker->text(50);
    //$post->slug = $faker->slug;
    $post->description = $faker->realText();
    $post->user_id = $faker->numberBetween(1, 50);
    $post->save();

    $post->tags()->attach($faker->randomElements(range (1, 10), 3));
}