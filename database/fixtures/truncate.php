<?php
require_once './app.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::statement('SET foreign_key_checks = 0');
foreach (['users', 'posts', 'tags', 'post_tags'] as $table) {
    Capsule::table($table)->truncate();
}
Capsule::statement('SET foreign_key_checks = 1');