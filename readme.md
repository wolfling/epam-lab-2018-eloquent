**Composer**

`php composer install`

**Migrations**

`php ./database/migrations/migration_up.php`

`php ./database/migrations/migration_down.php`
 
**Fixtures**

`php ./database/fixtures/truncate.php`

`php ./database/fixtures/faker.php`

**Run tests script**

`php ./index.php`