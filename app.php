<?php
require_once './vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Dotenv\Dotenv;
use app\Models\User;
use app\Observers\UserObserver;

(new Dotenv(__DIR__))->load();

$capsule = new Capsule();

$capsule->addConnection([
    'driver'    => env('MYSQL_DRIVER'),
    'host'      => env('MYSQL_HOST'),
    'username'  => env('MYSQL_USER'),
    'password'  => env('MYSQL_PASS'),
    'database'  => env('MYSQL_DATABASE'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

// Set the event dispatcher used by Eloquent models... (optional)
$capsule->setEventDispatcher(new Dispatcher(new Container));

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

// Registering Eloquent Observers
//Capsule::listen(function($query) {
//    echo sprintf("%s\n", $query->sql);
//});

//User::observe(UserObserver::class);