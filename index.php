<?php
require_once './app.php';

use app\Models\User;
use app\Models\Post;
use app\Models\Tag;

$users = User::with('posts')->get();

foreach ($users as $user) {
    echo sprintf(
        "%s - %d posts\n",
        $user->name,
        $user->posts->count()
    );
}
