<?php

namespace app\Observers;

use app\Models\User;

class UserObserver
{
    public function retrieved(User $user)
    {
        echo "User retrieved\n";
    }

    public function creating(User $user)
    {
        echo "creating";

        $user->name = ucfirst($user->name);
    }
}