<?php

namespace app\Models;

use Carbon\Carbon;
use \Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property Carbon $birthday
 * @property $gender
 * @property Post[]|Collection $posts
 */
class User extends Model
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    protected $fillable = ['name', 'birthday', 'gender'];

    protected $casts = [
        'birthday' => 'date',
    ];

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeMale($query)
    {
        return $query->where('gender', self::GENDER_MALE);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeFemale($query)
    {
        return $query->where('gender', self::GENDER_FEMALE);
    }

    /**
     * User Has Many Posts
     *
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}