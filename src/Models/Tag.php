<?php

namespace app\Models;

use \Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class Tag extends Model
{
    protected $fillable = ['name'];
}